package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_FindLead extends ProjectMethods{


	//@BeforeTest
	public void setData() {
		testCaseName="TC003_FindLead";
		testDescription="Find a lead";
		testNodes="Leads";
		author="Manica";
		category="smoke";
		//dataSheetName="TC001";
		dataSheetName="TC002";
	}
	//@Test(dataProvider="loginDetails")
	public void findLead(String username, String password,String leadName) {

		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.goToLeadPage()
		.clickLead()
		.clickFindLeadLink()
		.enterLeadName(leadName)
		.clickFindLead()
		.clickFirstResultName();
	
		
	}
}

