package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC004_EditLead extends ProjectMethods{

	//@BeforeTest
	public void setData() {
		testCaseName="TC004_EditLead";
		testDescription="Edit a lead";
		testNodes="Leads";
		author="Manica";
		category="smoke";
		dataSheetName="TC001";
		dataSheetName2="TC002";
	}
	//@Test(dataProvider="loginDetails")
	public void findLead(String username, String password, String fname, String lname, String compName,String leadName,String updateCompName) {

		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.goToLeadPage()
		.clickLead()
		.clickFindLeadLink()
		.enterLeadName(leadName)
		.clickFindLead()
		.clickFirstResultName()
		.clickEditButton()
		.updateCompanyName(updateCompName)
		.clickUpdate();
	}

}
