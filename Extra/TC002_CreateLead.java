package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;

public class TC002_CreateLead extends ProjectMethods {

	
	//@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="Create a new lead";
		testNodes="Leads";
		author="Manica";
		category="smoke";
		dataSheetName="TC001";
		//dataSheetName2="TC002";
	}
	
	
	//@Test(dataProvider="loginDetails")
	public void login(String username, String password, String fname, String lname, String compName) {
		
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.goToLeadPage()
		.clickLead()
		.clickCreateLeadLink()
		.enterFirstName(fname)
		.enterLastName(lname)
		.enterCompanyName(compName)
		.clickCreateLead();
	}
		
		
	}


