package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods{

	
	@BeforeTest
	public void setData() {
		testCaseName="TC001_LoginLogout";
		testDescription="Login TestLeaf Page";
		testNodes="Leads";
		author="Manica";
		category="smoke";
		dataSheetName="TC001";
	}
	
	
	@Test(dataProvider="loginDetails")
	public void login(String username, String password,String fname, String lname, String compName) {
		
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.goToLeadPage()
		.clickLead()
		.clickCreateLeadLink()
		.enterFirstName(fname)
		.enterLastName(lname)
		.enterCompanyName(compName)
		.clickCreateLead();
		
		
	}
}
