package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	
	public EditLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="updateLeadForm_companyName") WebElement eleUpdateCompanyName;
	public EditLeadPage updateCompanyName(String data) {
		clearAndType(eleUpdateCompanyName, data);
		return this;
	}
	
	@FindBy(how = How.XPATH,using="//button[text() = 'Find Leads']") WebElement eleUpdate;
	public ViewLeadPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadPage();
	}
}
