package com.framework.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods{


	public FindLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 

	}
	@FindBy(how = How.ID_OR_NAME,using="firstName") WebElement eleFirstName;
	public FindLeadPage enterLeadName(String data) {
		clearAndType(eleFirstName, data);
		return this;
		
	}
	@FindBy(how = How.XPATH,using="//button[text() = 'Find Leads']") WebElement eleFindLeadButton;
	public FindLeadPage clickFindLead() {
		click(eleFindLeadButton);
		return this;
		
	}
	@FindBy(how = How.XPATH,using="//div[@class= 'x-grid3-cell-inner x-grid3-col-partyId']/./a") WebElement eleFirstResultName;
	public ViewLeadPage clickFirstResultName() {
		click(eleFirstResultName);
		return new ViewLeadPage();
	}
}
