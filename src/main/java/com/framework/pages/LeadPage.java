package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LeadPage extends ProjectMethods {
	

	public LeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	@FindBy(how=How.LINK_TEXT,using="Create Lead") WebElement eleCreateLeadLink;
	@FindBy(how=How.LINK_TEXT,using="Find Leads") WebElement eleFindLeadLink;
	public CreateLeadPage clickCreateLeadLink() {
		click(eleCreateLeadLink);
		return new CreateLeadPage();
	}
	
	public FindLeadPage clickFindLeadLink() {
		click(eleFindLeadLink);
		return new FindLeadPage();
	}
	
}
