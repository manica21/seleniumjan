package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeadPage extends ProjectMethods {
	

	public MyLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	@FindBy(how=How.LINK_TEXT,using="Leads") WebElement eleLeadLink;
	public LeadPage clickLead() {
		click(eleLeadLink);
		return new LeadPage();
	}
	
}
